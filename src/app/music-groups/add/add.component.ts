import {Component, OnInit} from '@angular/core';
import {MusicGroup} from "../model/MusicGroup";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styles: [
  ]
})
export class AddComponent implements OnInit{
  musicGroup: MusicGroup|undefined;

  constructor() {
  }

  ngOnInit(): void {
    this.musicGroup = new MusicGroup();
  }
}
