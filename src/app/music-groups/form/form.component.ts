import {Component, Input, OnInit} from '@angular/core';
import {FormControl, NgModel} from "@angular/forms";
import * as moment from "moment";
import {Moment} from "moment";
import {MatDatepicker, MatDatepickerModule} from "@angular/material/datepicker";
import {MusicGroup} from "../model/MusicGroup";
import {Router} from "@angular/router";
import {ApiService} from "../api.service";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styles: [
  ]
})

export class FormComponent implements OnInit {
  @Input() musicGroup: MusicGroup;

  constructor(private router: Router, private api: ApiService) {
  }

  ngOnInit(): void {
    if (this.musicGroup.startedAt) {
      this.musicGroup.strStartedAt = String(new Date(this.musicGroup.startedAt).getFullYear());
    } else {
      this.musicGroup.strStartedAt = '2000';
    }

    if (this.musicGroup.endedAt) {
      this.musicGroup.strEndedAt = String(new Date(this.musicGroup.endedAt).getFullYear());
    } else {
      this.musicGroup.strEndedAt = '';
    }
  }

  onChangeDate() {
    if (this.musicGroup.strStartedAt !== '') {
      this.musicGroup.startedAt = new Date(+this.musicGroup.strStartedAt, 1, 1);
    }

    if (this.musicGroup.strEndedAt !== '') {
      this.musicGroup.endedAt = new Date(+this.musicGroup.strEndedAt, 1, 1);
    } else {
      this.musicGroup.endedAt = null;
    }
  }

  onSubmit(): void {

    if (this.router.url.includes('add')) {
      this.api.addGroup(this.musicGroup)
          .subscribe((group: Object|undefined) => {
            let g = <MusicGroup|undefined>group;
            this.router.navigate(['/view', g?.id]);
          });
    } else {
      this.api.updateGroup(this.musicGroup)
          .subscribe(() => this.router.navigate(['/view', this.musicGroup.id]));
    }

  }
}