import { Injectable } from '@angular/core';
import {MusicGroup} from "./model/MusicGroup";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, of, tap} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl: string = 'http://gammang_back.localhost/api';

  private httpOptions = {
      headers: new HttpHeaders({
          'accept': 'application/json'
      })
  };

  constructor(private http: HttpClient) { }

    getGroupsList(): Observable<MusicGroup[]> {

        return this.http.get<MusicGroup[]>(`${this.apiUrl}/music_groups`, this.httpOptions).pipe(
            tap((response) => console.table(response)),
            catchError((error) => {
                console.error(error);
                return of([]);
            })
        );
    }

    getGroupById(id: number): Observable<MusicGroup|undefined> {

        return this.http.get<MusicGroup>(`${this.apiUrl}/music_groups/${id}`, this.httpOptions).pipe(
            tap((response) => console.table(response)),
            catchError((error) => {
                console.error(error);
                return of(undefined);
            })
        );
    }

    deleteGroupById(id: number): Observable<Object|null> {
        return this.http.delete(`${this.apiUrl}/music_groups/${id}`).pipe(
            tap((response) => console.log(response)),
            catchError((error) => {
                console.error(error);
                return of(null);
            })
        );
    }

    updateGroup(musicGroup: MusicGroup): Observable<Object|undefined> {
        return this.http.put(`${this.apiUrl}/music_groups/${musicGroup.id}`, musicGroup, this.httpOptions).pipe(
            tap((response) => console.log(response)),
            catchError((error) => {
                console.error(error);
                return of(undefined);
            })
        )
    }

    addGroup(musicGroup: MusicGroup): Observable<Object|undefined> {
        return this.http.post(`${this.apiUrl}/music_groups`, musicGroup, this.httpOptions).pipe(
            tap((response) => console.log(response)),
            catchError((error) => {
                console.error(error);
                return of(undefined);
            })
        )
    }
}
