import { Component } from '@angular/core';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styles: [
  ]
})
export class UploadComponent {
  private srcResult: string|ArrayBuffer;
  private requiredMimeType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';


  onFileSelected(): void {
    const inputNode: any = document.querySelector('#file');

    if (typeof (FileReader) !== 'undefined') {
      const reader = new FileReader();

      reader.onload = (e: any) => {
        this.srcResult = e.target.result;
      };

      if (inputNode.files[0].type === this.requiredMimeType) {
        reader.readAsArrayBuffer(inputNode.files[0]);
      } else {
        alert('Merci de choisir un fichier au format XLSX');
      }
    }
  }

  onSubmit(): void {
    const file = new Blob([this.srcResult], {type: this.requiredMimeType});
    console.log(file);
  }

}
