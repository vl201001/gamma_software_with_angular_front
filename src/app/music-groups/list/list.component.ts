import {Component, OnInit} from '@angular/core';
import {ApiService} from "../api.service";
import {MusicGroup} from "../model/MusicGroup";
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styles: [
  ]
})

export class ListComponent implements OnInit {
  listGroups: MusicGroup[];
  columnsToDisplay =  ['actions', 'name', 'origin', 'city', 'startedAt', 'endedAt', 'founders', 'nbMembers', 'musicType', 'description'];

  constructor(private api: ApiService, private router: Router) {
  }

  ngOnInit(): void {
    this.api.getGroupsList()
        .subscribe(groupsList => this.listGroups = groupsList);
  }

  deleteMusicGroup(id: number): void {
    this.api.deleteGroupById(id)
        .subscribe(() => {
          this.api.getGroupsList()
              .subscribe(groupsList =>this.listGroups = groupsList);
        });
  }

}
