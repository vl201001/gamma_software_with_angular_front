import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import {RouterModule, Routes} from "@angular/router";
import {MatTableModule} from "@angular/material/table";
import { ViewComponent } from './view/view.component';
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { FormComponent } from './form/form.component';
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from "@angular/material/datepicker";
import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';
import { UploadComponent } from './upload/upload.component';
import {MatCardModule} from "@angular/material/card";

const musicGroupsRoutes: Routes = [
    { path: 'list', component: ListComponent },
    { path: 'add', component: AddComponent },
    { path: 'view/:id', component: ViewComponent },
    { path: 'edit/:id', component: EditComponent },
]

@NgModule({
  declarations: [
    ListComponent,
    ViewComponent,
    FormComponent,
    EditComponent,
    AddComponent,
    UploadComponent
  ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(musicGroupsRoutes),
        MatTableModule,
        MatIconModule,
        MatButtonModule,
        MatInputModule,
        MatDatepickerModule,
        ReactiveFormsModule,
        MatCardModule
    ]
})
export class MusicGroupsModule { }
