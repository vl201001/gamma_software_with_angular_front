export class MusicGroup {
    id: number;
    name: string;
    origin: string;
    city: string;
    startedAt: Date;
    strStartedAt: string;
    endedAt: Date|null;
    strEndedAt: string;
    founders: string;
    nbMembers: number;
    musicType: string;
    description: string;
}