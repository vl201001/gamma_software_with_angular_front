import {Component, OnInit} from '@angular/core';
import {MusicGroup} from "../model/MusicGroup";
import {ActivatedRoute} from "@angular/router";
import {ApiService} from "../api.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styles: [
  ]
})
export class EditComponent implements OnInit{
  musicGroupId: number;
  musicGroup: MusicGroup|undefined;

  constructor(private route: ActivatedRoute, private api: ApiService) {
  }

  ngOnInit(): void {
    const urlId: string|null = this.route.snapshot.paramMap.get('id');

    if (urlId) {
      this.musicGroupId = +urlId;
      this.api.getGroupById(this.musicGroupId)
          .subscribe(group => this.musicGroup = group);
    }
  }
}
