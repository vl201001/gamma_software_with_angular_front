import { Component } from '@angular/core';
import {MatCardModule} from '@angular/material/card';

@Component({
  selector: 'app-page-not-found',
  template: `
    <mat-card>
      <mat-card-header>
        <mat-card-title>Erreur 404 !</mat-card-title>
      </mat-card-header>
      <mat-card-content>La page recherchée n'existe pas</mat-card-content>
      <mat-card-actions>
        <a routerLink="/"><button mat-button>ACCUEIL</button></a>
      </mat-card-actions>
    </mat-card>
  `,
})
export class PageNotFoundComponent {

}
