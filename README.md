# Projet gamma_software_with_angular_front

Partie front du projet avec Angular.

Pour aller plus vite sur la partie design, j'ai fait le choix d'utiliser [Angular Material UI](https://material.angular.io/)

## Context du projet

Projet consistant à importer un fichier Excel contenant des groupes de musique.

Le projet contient :
- [ ] Un formulaire d'import pour le fichier Excel
- [ ] Une interface pour accéder à la liste des groupes importés
- [ ] Un formulaire d'ajout / modification de groupe de musique
- [ ] Un bouton permettant de supprimer les groupes de musique
- [ ] Une fiche de détail pour les groupes de musique

## Non inclus dans le projet

Voici les fonctionnalités non demandées pour le projet qu'il serait judicieux de rajouter :

- Un système d'authentification (JWT) avec une gestion des utilisateurs
- Une intégration du responsive design pour un support de l'affichage sur mobile (même si Angular Material UI le supprote déjà en partie)
- Une récupération des logs d'import